historial=[]
def sumaS():
    n1 = float(input("ingrese un numero: "))
    numero = "r"
    while numero != "=":
        numero = input("ingrese otro numero para la operacion e ingrese = para el resultado: ")
        if numero != "=":
            n2 = float(numero)
            n1 += n2
            R_suma = n1
        elif numero == "=":
            print(R_suma)
            historial.append(R_suma)
            break
        else:
            print("ingrese un numero o el igual")
    return R_suma

def restaS():
    n1 = float(input("ingrese un numero: "))
    numero = "r"
    while numero != "=":
        numero = input("ingrese otro numero para la operacion e ingrese = para el resultado: ")
        if numero != "=":
            n2 = float(numero)
            n1 -= n2
            R_resta = n1
        elif numero == "=":
            print(R_resta)
            historial.append(R_resta)
            break
        else:
            print("ingrese un numero o el igual")
    return  R_resta

def diviS():
    n1 = float(input("ingrese un numero: "))
    numero = "r"
    while numero != "=":
        numero = input("ingrese otro numero para la operacion e ingrese = para el resultado: ")
        if numero != "=":
            n2 = float(numero)
            n1 /= n2
            R_division = n1
        elif numero == "=":
            print(R_division)
            historial.append(R_division)
            break
        else:
            print("ingrese un numero o el igual")
    return  R_division

def multiS():
    n1 = float(input("ingrese un numero: "))
    numero = "r"
    while numero != "=":
        numero = input("ingrese otro numero para la operacion e ingrese = para el resultado: ")
        if numero != "=":
            n2 = float(numero)
            n1 = n1 * n2
            R_multi = n1
        elif numero == "=":
            print(R_multi)
            historial.append(R_multi)
            break
        else:
            print("ingrese un numero o el igual")
    return  R_multi
 
def CalcuS():
  opcion2 = 1
  while opcion2 != "=":
    print("Elija una Operacion:")
    print("1: Suma")
    print("2: Resta")
    print("3: Multiplicacion")
    print("4: Division")
    print("=: Salir")
    opcion2 = input()
    if opcion2 == "1":
        c = sumaS()
    elif opcion2 == "2":
        c = restaS()
    elif opcion2 == "3":
        c = multiS()
    elif opcion2 == "4":
        c = diviS()
    elif opcion2 == "=":
        break
    else:
        print("ingrese una de la opciones")
  return sumaS
  
def sumaF():
    num1 = int(input("ingrese un numerador: "))
    deno1 = int(input("ingrese un denominador: "))
    print(f"{num1}/{deno1}")
    num2 = int(input("ingrese un numerador: "))
    deno2 = int(input("ingrese un denominador: "))
    print(f"{num2}/{deno2}")
    if deno1 == deno2:
        denoR = deno1
        numR = num1 + num2
    else:
        denoR = deno1 * deno2
        numR = (num1*deno2)+(num2*deno1)
    num3 = "s"
    while num3 != "=":
        num3 = input("ingrese otro numerador o elija = para ver el resultado: ")
        if num3 != "=":
            num3 = int(num3)
            deno3 = int(input("ingrese otro denominador: "))
            print(f"{num3}/{deno3}")
            if denoR == deno3:
                denoR = denoR
                numR = numR + num3
            else:
                numR = (numR*deno3)+(num3*denoR)
                denoR = denoR * deno3
        elif num3 == "=":
            break
        else:
            print("Ingrese un numero o el simbolo =")
    while numR%2 == 0 and denoR%2 == 0:
        numR = numR // 2
        denoR = denoR // 2
    while numR%3 == 0 and denoR%3 == 0:
        numR = numR // 3
        denoR = denoR // 3
    historial.append((f"{numR}/{denoR}"))
    print(f"Resultado: {numR}/{denoR}")
    return numR

def restaF():
    num1 = int(input("ingrese un numerador: "))
    deno1 = int(input("ingrese un denominador: "))
    print(f"{num1}/{deno1}")
    num2 = int(input("ingrese un numerador: "))
    deno2 = int(input("ingrese un denominador: "))
    print(f"{num2}/{deno2}")
    if deno1 == deno2:
        denoR = deno1
        numR = num1 - num2
    else:
        denoR = deno1 * deno2
        numR = (num1*deno2)-(num2*deno1)
    num3 = "s"
    while num3 != "=":
        num3 = input("ingrese otro numerador o elija = para ver el resultado: ")
        if num3 != "=":
            num3 = int(num3)
            deno3 = int(input("ingrese otro denominador: "))
            print(f"{num3}/{deno3}")
            if denoR == deno3:
                denoR = denoR
                numR = numR - num3
            else:
                numR = (numR*deno3)-(num3*denoR)
                denoR = denoR * deno3
        elif num3 == "=":
            while numR%2 == 0 and denoR%2 == 0:
                numR = numR // 2
                denoR = denoR // 2
            while numR%3 == 0 and denoR%3 == 0:
                numR = numR // 3
                denoR = denoR // 3
            break
        else:
            print("Ingrese un numero o el simbolo =")
    print(f"Resultado: {numR}/{denoR}")
    historial.append((f"{numR}/{denoR}"))
    return numR

def multiF():
    num1 = int(input("ingrese un numerador: "))
    deno1 = int(input("ingrese un denominador: "))
    print(f"{num1}/{deno1}")
    num2 = "s"
    while num2 != "=":
        num2 = input("ingrese otro numerador o elija = para ver el resultado: ")
        if num2 != "=":
            num2 = int(num2)
            deno2 = int(input("ingrese otro denominador: "))
            print(f"{num2}/{deno2}")
            deno1 *= deno2
            num1 *= num2
        elif num2 == "=":
            while num1%2 != 1 or deno1%2 != 1:
                num1 = num1 // 2
                deno1 = deno1 // 2
                while numR%3 == 0 and denoR%3 == 0:
                    numR = numR // 3
                    denoR = denoR // 3
            historial.append((f"{num1}/{deno1}"))
            print(f"{num1}/{deno1}")
            break
        else:
            print("ingrese un numero o el simbolo =")
    return num1

def diviF():
  num1 = int(input("ingrese un numerador: "))
  deno1 = int(input("ingrese un denominador: "))
  print(f"{num1}/{deno1}")
  num3 = int(input("ingrese un numerador: "))
  deno3 = int(input("ingrese un denominador: "))
  print(f"{num3}/{deno3}")
  num1 *= deno3
  deno1 *= num3
  num1 , deno1 = deno1 , num1
  num2 = "s"
  while num2 != "=":
      num2 = input("ingrese otro numerador o elija = para ver el resultado: ")
      if num2 != "=":
          num2 = int(num2)
          deno2 = int(input("ingrese otro denominador: "))
          print(f"{num2}/{deno2}")
          num1 *= deno2
          deno1 *= num2
          num1 , deno1 = deno1 , num1
      elif num2 == "=":
          while num1%2 != 1 or deno1%2 != 1:
                num1 = num1 // 2
                deno1 = deno1 // 2
                while numR%3 == 0 and denoR%3 == 0:
                    numR = numR // 3
                    denoR = denoR // 3
          print(f"Resultado: {deno1}/{num1}")
          historial.append((f"{deno1}/{num1}"))
          break
      else:
          print("Ingrese un numero o el simbolo =")
  return num1


def CalcuF():
  opcion2 = 1
  while opcion2 != "=":
    print("Elija una Operacion:")
    print("1: Suma")
    print("2: Resta")
    print("3: Multiplicacion")
    print("4: Division")
    print("=: Salir")
    opcion2 = input()
    if opcion2 == "1":
        r = sumaF()
    elif opcion2 == "2":
        r = restaF()
    elif opcion2 == "3":
        r = multiF()
    elif opcion2 == "4":
        r = diviF()
    elif opcion2 == "=":
        break
    else:
        print("ingrese una de la opciones")
  return sumaF


def calc_conversiones():
    num = []
    decimal = int(input(f"Ingrese un numero decimal: "))
    print(f"A que sistema quiere convertir el numero?")
    print(f"Ingrese bin para binario, hex para hexadecimal o oct para octal")
    act = input("")
    if act == 'bin':
        while decimal >= 1:
            mod = decimal % 2
            num.insert(0, mod)
            decimal = decimal // 2
            conc = "".join(str(i) for i in num)
        print(f"Resultado: {conc}")
        historial.append(conc)
    elif act == 'hex':
        while decimal >= 1:
            mod = decimal % 16
            if mod == 10:
                mod = "A"
            elif mod == 11:
                mod = "B"
            elif mod == 12:
                mod = "C"
            elif mod == 13:
                mod = "D"
            elif mod == 14:
                mod = "E"
            elif mod == 15:
                mod = "F"
            num.insert(0, mod)
            decimal = decimal // 16
            conc = "".join(str(i) for i in num)
        print(f"Resultado: {conc}")
        historial.append(conc)
    elif act == 'oct':
        while decimal >= 1:
            mod = decimal % 8
            num.insert(0, mod)
            decimal = decimal // 8
            conc = "".join(str(i) for i in num)
        print(f"Resultado: {conc}")
        historial.append(conc)
    
    return conc


a = 0
while a == 0:
    encendido = "z"
    while encendido != "On":
        encendido = input("ingrese On para iniciar el programa: ")
        if encendido.lower() != "on":
            print("vuelva a intentarlo")
        else:
            break
    print("")
    print("----------Opciones----------")
    print("")
    opcion = 1
    while opcion != 5:
        print("Elija una Opcion:")
        print("1: Calculadora Simple")
        print("2: Calculadora Fraccionaria")
        print("3: Conversion de Numeros")
        print("4: Historial")
        print("5: Salir")
        opcion = int(input("Opcion: "))
        if opcion == 1:
            print("")
            print("----------Calculadora Simple----------")
            print("")
            CalcuS()
        elif opcion == 2:
            print("")
            print("----------Calculadora Fraccionaria----------")
            print("")
            CalcuF()
        elif opcion == 3:
            print("")
            print("----------Conversion de Numeros----------")
            print("")
            calc_conversiones()
        elif opcion == 4:
            print("")
            print("----------Tu Historial----------")
            print("")
            print(f"{historial [::-1]}")
        elif opcion == 5:
            print("")
            historial.clear()
            print("Calculadora apagada")
            break
        else: 
            print("ingrese una de las opciones correctas")
